# Mini-debconf à Toulouse !

L'association Debian France organise une mini-debconf à Toulouse. Cet évènement accueillera toutes les personnes intéressées par le projet Debian avec une série de conférences et d'ateliers autour de ce thème.

Cette mini-debconf aura lieu en même temps et au même endroit que le Capitole du Libre les 18 et 19 novembre 2017 à l'ENSEEIHT 

Les conférences et ateliers seront programmés sur la même grille que les conférences du Capitole du Libre. Il sera, ainsi, aisé de basculer de la mini-debconf au capitole du libre et inversement bien sur ;)


### Programme

Les propositions de conférences sont ouvertes, vous pouvez vous inscrire par mail à  minidebconf[at]france[dot]debian[dot]net et sur la page wiki.
Date limite pour soumettre une conférence : le 30 septembre 2017

Nous aimerions avoir la moitié des conférences en anglais, les anglophones sont donc les bienvenus !


### Dates et lieu

Les 18 et 19 novembre 2017 à ENSEEIHT: 26 rue Pierre-Paul Riquet à Toulouse


### Inscription

Ajoutez vous sur [cette page wiki](https://wiki.debian.org/DebianEvents/fr/2017/Toulouse) ou contactez les organisateurs.

### Sponsors

Contactez nous pour apparaitre ici ;)

### Organisateurs

* Denis Briand - <<debian[at]denis-briand[dot]fr>>
* Alexandre Delanoë - <<anoe[at]debian[dot]org>>



# Events

## Mini Debian Conference

The Debian France Association organizes its 2017 Mini-DebConf in Toulouse. This event will welcome everyone with an interest in the Debian project for a series of talks and workshops about Debian. Allowing users, contributors and developers to meet each other is one of the main goal of this event. We are especially looking forward to meeting Debian developers from around the world.

### Schedule

*Talks are in french or english.*

[[!inline pages="evenements/minidebconf2016/programme" raw="yes" rss="no"]]

### Location


The Mini-DebConf Toulouse 2017 will take place on **November 18th and 19th, 2017** in **Toulouse** during the event "Capitole du Libre" at the ENSEEIHT school : 26 rue Pierre-Paul Riquet, Toulouse FRANCE.


### Registration

Please add yourself on [this wiki page](https://wiki.debian.org/DebianEvents/fr/2017/Toulouse) or get in touch with organizers below.

### Sponsors

Just ask us to be here, you are welcome!

### Organizers

* Denis Briand - <<debian[at]denis-briand[dot]fr>>
* Alexandre Delanoë - <<anoe[at]debian[dot]org>>

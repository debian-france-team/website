[[!meta title="Deuxième meetup à Paris"]]

Deuxième meetup à Paris
=======================

Le second Meetup organisé par l'association Debian France aura lieu le mardi 10  décembre prochain à partir de 18h30.

Il sera hébergé par IRILL (proche de place d'italie à Paris):
http://www.irill.org/about/access

Le programme, si il y en a un, sera annoncé plus tard.
Si vous voulez proposer une présentation (rapide ou longue), n'hésitez pas à contacter Sylvestre Ledru - <<sylvestre@debian.org>>.

L'évènement est sponsorisé par <a href="http://www.logilab.fr/">Logilab</a>.

Inscriptions:

* <a href="http://www.meetup.com/Debian-France/events/153675112/">Meetup.com</a>
* <a href="https://wiki.debian.org/DebianEvents/fr/2013/Meetup2">Wiki Debian</a>



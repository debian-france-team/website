[[!meta title="Debian France aux Solutions Linux/Open Source 2013"]]

Stand Debian France aux Solutions Linux/Open Source 2013
========================================================

Le salon [Solutions Linux/Open Source](http://www.solutionslinux.fr/) aura lieu
les 28 et 29 mai 2013, au [CNIT à La
Défense](http://www.openstreetmap.org/?mlat=48.8927&mlon=2.2395&zoom=15).

Ce salon, essentiellement destiné aux professionnels, comporte comme chaque
année un « village associatif » qui regroupe différentes associations du monde
du logiciel libre. Debian France y tiendra un stand, où vous pourrez :

* vous informer sur le système d'exploitation et la communauté Debian ;
* acheter des accessoires pour afficher votre attachement à Debian : polos,
  autocollants…

[[Stand Debian France aux SL 2011|stand_2011.jpg]]

Si vous souhaitez aider, [nous cherchons quelques
volontaires](http://wiki.debian.org/DebianEvents/fr/2013/SL) pour tenir le
stand.

Pour plus d'informations :

* [Solutions Linux/Open Source](http://www.solutionslinux.fr/) ;
* [stand Debian France](http://wiki.debian.org/DebianEvents/fr/2013/SL) ;
* [Debian France](http://france.debian.net/) ;
* [liste de distribution](https://france.debian.net/mailman/listinfo/asso) ;
* [salon IRC](irc://irc.oftc.net/#debian-france).

[[!meta title="Meetup 2019/1 - 27 Juin - Bordeaux"]]

Meetup du 27 Juin à Bordeaux
============================

Nous vous invitons à nous rejoindre pour notre premier Meetup sur Bordeaux !

Il se déroulera le 27 Juin 2019 à partir de 18h30.

Le thème sera le suivant : « Présentation de la distribution GNU/Linux Debian. Son organisation et ses projets. »

Une page sur le [Wiki Debian](https://wiki.debian.org/DebianEvents/fr/2019/Meetup1) est dédiée à l'évènement.

 * [Lien Meetup de l'évènement](https://meetu.ps/e/GNSQ3/CgPk7/f)

Merci de vous inscrire à l'évènement pour que nous puissions prévoir votre accueil dans les meilleures conditions.

[[!meta title="Meetup de Juin � Paris"]]

Meetup � Paris
==============

Le meetup sponsoris� par Mozilla a accueilli une quarantaine de personnes.

<img src="/posts/2014/Meetup_Paris_06_2014/photo.jpg" />

Les pr�sentations �taient:

* Axel Viala - Pr�sentation de Mozilla - <a href="/posts/2014/Meetup_Paris_06_2014/Intro_Mozilla.pdf">Slides</a>
* Sylvestre Ledru - Comment contribuer � Debian ? - <a href="/posts/2014/Meetup_Paris_06_2014/how-to-contribute.pdf">Slides</a> - <a href="https://air.mozilla.org/meetup-debian-3-mozilla-contributing-to-debian-with-sylvestre-ledru/">Vid�o sur Air Mozilla</a>
* Nicolas Dandrimont - Google Summer of Code 2014 @Debian - <a href="http://perso.crans.org/dandrimont/talks/debianfr-meetup3/gsoc14.html#/">Slides</a> - <a href="https://air.mozilla.org/meetup-debian-3-mozilla-gsoc-in-debian-with-nicolas-dandrimont/">Vid�o sur Air Mozilla</a>
* Jean-Baptiste Favre - Comment BlaBlaCar utilise Debian ? - <a href="/posts/2014/Meetup_Paris_06_2014/debian-blablacar-20140604.pdf">Slides</a> - <a href="https://air.mozilla.org/meetup-debian-3-mozilla-using-debian-at-blablacar-by-jean-baptiste-favre/">Vid�o sur Air Mozilla</a>

# Les bénévoles de Debian France

## Le bureau <<bureau@france.debian.net>>

 - Président : Pierre-Elliott Bécue <<president@france.debian.net>>
 - Président Adjoint : Alban Vidal
 - Trésorier : Jean-Pierre Giraud <<tresorier@france.debian.net>>
 - Secrétaire : Jean-Philippe Mengual <<secretaire@france.debian.net>>
 - Secrétaire Adjoint : Quentin Lejard

## Le conseil d'administration <<ca@france.debian.net>>

Il est constitué du bureau et de :

 - Georges Khaznadar
 - Grégory Colpart
 - William Bonnet

## Délégation des pouvoirs

 - Administration du serveur france.debian.net : Bureau, Gregory Colpart
 - Administration des listes de diffusion : Bureau
 - Administration du site Web : Bureau, Gregory Colpart

## Les autres bénévoles

 - [Mehdi Dogguy](http://dogguy.org) a participé à l'organisation des deux
   premières mini-debconfs.
 - [Luc Didry](https://www.fiat-tux.fr/) a organisé l'achat des stickers détourés.
 - Vincent Bernat a aidé sur le stand à Solutions Linux.
 - Thibaut Paumard a aidé sur le stand à Solutions Linux.
 - François-Régis a aidé sur le stand à Open World Forum.
 - Michael Bonfils a aidé sur le stand à Open World Forum.
 - William Bonnet a aidé sur le stand à Open World Forum.
 - Léo Cavaillé a aidé sur le stand lors de la DebConf 13.
 - Jean-Baptiste Perrier a aidé sur le stand lors de la DebConf 13.
 - Et sûrement d'autres qui ont été oubliés. Si c'est votre cas, écrivez-nous à
   <ca@france.debian.net> pour que nous corrigions cet affreux oubli!
